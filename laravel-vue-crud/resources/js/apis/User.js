import Api from './Api';

export default {
    name: 'User',
    token() {
        return axios.get('/sanctum/csrf-cookie');
    },
    register(form) {
        return Api.post('/register', form);
    },
    login(form) {
        return Api.post('/login', form);
    },
    logout() {
        return Api.post('/logout');
    },
}