
import NotFound from './components/NotFound.vue';
import Home from './components/Home.vue';
import Dashboard from './components/Dashboard.vue';
import Register from './components/Register.vue';
import Login from './components/Login.vue';
import Password from './components/Password.vue';
import Logout from './components/Logout.vue';

import Users from './components/Users.vue';
import AddUser from './components/AddUser.vue';

export const routes = [
    {
        name: 'NotFound',
        path: '*',
        component: NotFound
    },
    {
        name: 'Home',
        path: '/',
        component: Home,
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('auth')) {
                next();
            } else {
                return next({ name: 'Login' })
            }
        }
    },
    {
        name: 'Dashboard',
        path: '/dashboard',
        component: Dashboard
    },
    {
        name: 'Users',
        path: '/dashboard/users',
        component: Users
    },
    {
        name: 'AddUser',
        path: '/dashboard/add/user',
        component: AddUser
    },
    {
        name: 'Register',
        path: '/register',
        component: Register
    },
    {
        name: 'Login',
        path: '/login',
        component: Login,
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('auth')) {
                return next({ name: 'Home' })
            } else {
                next();
            }
        }
    },
    {
        name: 'Password',
        path: '/password',
        component: Password
    },
    {
        name: 'Logout',
        path: '/logout',
        component: Logout
    },
];