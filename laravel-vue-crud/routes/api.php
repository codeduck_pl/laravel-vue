<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('password', [AuthController::class, 'resetPassword']);
Route::post('logout', [AuthController::class, 'logout']);

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/')->group(function () {
        Route::get('', [HomeController::class, 'index']);
    });
    // Route::prefix('/dashboard')->group(function () {
    //     Route::get('', [DashboardController::class, 'index']);
    //     Route::get('/users', [UserController::class, 'index']);
    //     Route::put('/add/user', [UserController::class, 'store']);
    //     Route::delete('/delete/user/{id}', [UserController::class, 'destroy']);
    // });
});

// add this when u be smarter - middleware('auth:sanctum')->
Route::prefix('/dashboard')->group(function () {
    Route::get('', [DashboardController::class, 'index']);
    Route::get('/users', [UserController::class, 'index']);
    Route::put('/add/user', [UserController::class, 'store']);
    Route::delete('/delete/user/{id}', [UserController::class, 'destroy']);
});
  